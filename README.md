# Servisoft

<p>
	Sistema de venta de vinos.
</p>

## Objetvivos Generales

<p>
	Administrar los ingresos de las ventas de vinos y piscos.
</p>

## Objetivos específicos

<p>
	Acceso de forma rápida.
	Controlar y realizar los gastos.
	Realizar los reportes de los formatos.
</p>

## Alcances

<p>
	Registro de los clientes.
	Registro y reporte.
</p>

## Requerimientos funcionales

<p>
	Mantenimiento de la tabla persona.
</p>

## Requerimientos no funcionales

<p>
	Mantener una navegación entendible y sencillo del sistema para el usuario que se registre.
</p>